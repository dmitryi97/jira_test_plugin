package com.askartec.plugin.utils;

import com.askartec.plugin.invoices.InvoiceDataModel;
import com.askartec.plugin.payments.PaymentDataModel;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import java.sql.*;
import java.util.ArrayList;


public class pg {
    public static ArrayList<InvoiceDataModel> getInvoicesByIssueKey(String issueKey) {
        String url = "jdbc:postgresql://192.168.0.185:5432/gazprom";
        String username = "postgres";
        String password = "postgres";
        Connection conn = null;

        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, username, password);

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT i.*, COALESCE(s.description, 'Неизвестная услуга') AS service_name, " +
                        "(" +
                        "SELECT json_agg(payments) " +
                        "FROM (" +
                                "SELECT p.txn_id, p.txn_date, p.pay_amount, COALESCE(p.sender, 'Неизвестный отправитель') AS sender " +
                                "FROM ps_replenishment_payment p " +
                                "WHERE p.account = i.account AND p.service_id = i.service_id" +
                            ") AS payments" +
                        ") AS payments " +
                    "FROM invoices_for_payment i " +
                    "LEFT JOIN rng_public_services s ON s.code = i.service_id " +
                    "WHERE issue_key = ?;");

            preparedStatement.setString(1, issueKey);
            ResultSet rs = preparedStatement.executeQuery();
            conn.close();

            // Собираем список счетов к оплате
            ArrayList<InvoiceDataModel> invoiceList = new ArrayList<InvoiceDataModel>();


            // Обрабатываем полученные строки
            while( rs.next() ){
                String id = rs.getString(1);
                String account = rs.getString(2);
                int serviceId = rs.getInt(3);
                float required_amount = rs.getFloat(4);
                int status = rs.getInt(5);
//                String issueKey = rs.getString(6);
                String serviceName = rs.getString(7);
                String jsonArrPayments = rs.getString(8);


                // Если есть хроника платежей, парсим ее в список
                ArrayList<PaymentDataModel> paymentHistory = new ArrayList<PaymentDataModel>();
                if (jsonArrPayments != null) {
                    JSONArray arrPayments = new JSONArray( rs.getString(8) );

                    // JSON массив платежей преобразуем в список
                    for (int i = 0; i < arrPayments.length(); i++) {
                        JSONObject rec = arrPayments.getJSONObject(i);

                        String txn_id = rec.getString("txn_id");
                        String txn_date = rec.getString("txn_date");
                        float pay_amount = (float)rec.getDouble("pay_amount");
                        String sender = rec.getString("sender");

                        paymentHistory.add(new PaymentDataModel(txn_id, account, txn_date, pay_amount, serviceId, serviceName, sender)  );
                    }
                }

                InvoiceDataModel invoiceItem = new InvoiceDataModel(id, serviceId, serviceName, required_amount, paymentHistory);
                invoiceList.add(invoiceItem);

                // Обернуть поля в класс который поддерживает интерфейс IssueAction
                // Запихать этот объект в List
            }

            if( invoiceList.size() == 0 ) {
                log.debug("Для задачи: "+ issueKey +" счета к оплате не найдены");
                return null;
            }
            else {
                log.debug("Успешно получен список счетов к оплате для задачи "+ issueKey);
                return invoiceList;
            }
        } catch (SQLException e) {
            log.error("Вознакла ошибка SQL выражения!");
            log.trace("Сообщение: "+ e.getMessage() );
            return null;
        } catch (JSONException e) {
            log.error("Вознакла ошибка JSON парсера!");
            log.trace("Сообщение: "+ e.getMessage() );
            return null;
        }
        catch (Exception e) {
            log.error("Вознакла не определенная ошибка!");
            log.trace("Сообщение: "+ e.getMessage() );
            e.printStackTrace();
            return null;
        }
    }
}
