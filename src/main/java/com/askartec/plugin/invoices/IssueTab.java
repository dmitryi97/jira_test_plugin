package com.askartec.plugin.invoices;

import com.askartec.plugin.utils.log;
import com.askartec.plugin.utils.pg;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.tabpanels.GenericMessageAction;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel3;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import com.google.common.collect.Lists;
import com.askartec.plugin.payments.PaymentDataModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



// Этот класс указывается в XML
public class IssueTab extends AbstractIssueTabPanel3 {
    @Override
    public boolean showPanel(ShowPanelRequest showPanelRequest) {
        return true;
    }



    @Override
    public List<IssueAction> getActions(GetActionsRequest getActionsRequest) {
        try {
            // Получаем ключ задачи
            String issueKey = getActionsRequest.issue().getKey();
            List<IssueAction> list = new ArrayList<>();

            // Получаем список счетов к оплате из БД
            ArrayList<InvoiceDataModel> invoiceListFromDB = pg.getInvoicesByIssueKey(issueKey);

            int count = 0;
            if(invoiceListFromDB != null) {
                while (invoiceListFromDB.size() > count) {
                    list.add( new InvoiceItem( invoiceListFromDB.get(count) ) );
                    count++;
                }
            }
            else {
                return Lists.newArrayList(
                    new GenericMessageAction("<div style=\"text-align:center;\">Для текущей задачи счета не найдены!</div>")
                );
            }

    //        list.add(
    //            new InvoiceItem(
    //                new InvoiceDataModel("21543", 0, "Тестовая услуга", 2850,
    //                    Lists.newArrayList(
    //                        new PaymentDataModel("102", "10041100", new Date(), 2500, 0, "Тестовая услуга", "Мегапей"),
    //                        new PaymentDataModel("107", "10041100", new Date(), 100, 0, "Тестовая услуга", "Мегапей"),
    //                        new PaymentDataModel("151", "10041100", new Date(), 250, 0, "Тестовая услуга", "Мегапей")
    //                    )
    //                )
    //            )
    //        );
    //
    //        list.add(
    //            new InvoiceItem(
    //                new InvoiceDataModel("21544", 0, "Тестовая услуга", 500,
    //                    Lists.newArrayList(
    //                        new PaymentDataModel("228", "050000822", new Date(), 350, 0, "Тестовая услуга", "Ракамакафо"),
    //                        new PaymentDataModel("239", "050000822", new Date(), 152, 0, "Тестовая услуга", "Оптима банк")
    //                    )
    //                )
    //            )
    //        );

                return list;
        }
        catch (Exception e) {
            log.error("В методе getActions вознакла ошибка!");
            log.trace("Сообщение: " + e.getMessage());
            return null;
        }
    }
}
