package com.askartec.plugin.invoices;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import java.util.Date;


public class InvoiceItem implements IssueAction {
    public InvoiceDataModel invoiceData;

    InvoiceItem(InvoiceDataModel data) {
        this.invoiceData = data;
    }

    public String getHtml() {
        StringBuilder preHistory = new StringBuilder();
        int count = 0;
        if(this.invoiceData.history != null) {
            while (this.invoiceData.history.size() > count) {
                preHistory.append("<div style=\"display: flex;\">" +
                        "<span style=\"flex: auto 1 0;\">"+ this.invoiceData.history.get(count).transactionId +"</span>" +
                        "<span style=\"flex: auto 1 0;\">"+ this.invoiceData.history.get(count).date +"</span>" +
                        "<span style=\"flex: auto 1 0;\">" +
                        "<span style=\" color: darkgreen; font-weight: 700;\">"+ this.invoiceData.history.get(count).amount +" сом</span>" +
                        "</span>" +
                        "<span style=\"flex: auto 1 0;\">" +
                        "<span>"+ this.invoiceData.history.get(count).sender +"</span>" +
                        "</span></div>");
                count++;
            }
        }
        else {
            preHistory.append("<div>Платежи не обнаружены!</div>");
        }

        String history = preHistory.toString();

        return "<article style=\"max-width:550px; font: normal 13px sans-serif;\">" +

                "<main style=\"background: #fbfbfb; padding: 0.3em 0.3em; margin-bottom: 0.8em; border: 1px dashed #ccc;\">" +
                "<section style=\"display: flex;\">" +
                "<div style=\"flex: auto;\">" +
                "<div style=\"padding-bottom:.3em;font:700 1.1em sans-serif;\">Счет на оплату № "+ this.invoiceData.id +"</div>" +
                "<div>" +
                "<span>Услуга ["+ this.invoiceData.serviceId +"]: <span style=\"font-weight: 700;\">" + this.invoiceData.serviceName +"</span></span>" +
                "</div>" +
                "<div>" +
                "<div>" +
                "<span>К оплате: <span style=\"" +
                "color: darkgreen;" +
                "font-weight: 700;\">" +
                this.invoiceData.amount + " сом</span>" +
                "</span>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div style=\"flex: auto;\">" +
                "<span style=\"" +
                "background: #ffbf00;" +
                "padding: .2em .6em;" +
                "border-radius: 3px;" +
                "color: #877631;" +
                "font-weight: 700;" +
                "float: right;" +
                "\">Не оплачен</span>" +
                "</div>" +
                "</section>" +

                "<section>" +
                "<div style=\"padding:.5em 0;\">" +
                "<span style=\"padding-bottom:.3em;font:700 1.1em sans-serif;\">История платежей:</span>" +
                "</div>" +
                history +
                "</section>" +
                "</main>" +

                "</article>";
    }

    public Date getTimePerformed() {
        // Вернуть дату платежа
        return new Date();
    }

    public boolean isDisplayActionAllTab() {
        return true;
    }

}
