package com.askartec.plugin.users;

import com.atlassian.jira.issue.tabpanels.GenericMessageAction;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel3;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import com.google.common.collect.Lists;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;





// Этот класс указывается в XML
public class IssueTab extends AbstractIssueTabPanel3 {
    @Override
    public boolean showPanel(ShowPanelRequest showPanelRequest) {
        return true;
    }




    @Override
    public List<IssueAction> getActions(GetActionsRequest getActionsRequest) {

        String url = "jdbc:postgresql://192.168.0.166:5432/optima_jira_mobile";
        String username = "postgres";
        String password = "postgres";

        try {
            Connection conn = DriverManager.getConnection(url, username, password);

            // Достаю данные с БД
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("SELECT login, email, created FROM users LIMIT 5;");
            conn.close();

            // Создаю список
            java.util.ArrayList<IssueAction> usersList = new java.util.ArrayList<IssueAction>();

            // Каждую запись оборачиваю в UserRow (который реализует IssueAction), и добавляю в список
            while( rs.next() ){
                String login = rs.getString(1);
                String email = rs.getString(2);
//                java.sql.Date created = rs.getDate(3);

                UserRow row = new UserRow(login, email);
                usersList.add( row );
            }


            System.out.println("Connected to the PostgreSQL server successfully.");



            return usersList;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return Lists.newArrayList(
            new UserRow("login1", "email@mail.com" ),
            new UserRow("login2", "email2@mail.com" )
        );

//        if (true) {
//            // НОРМ
//            return Lists.newArrayList(
//                new GenericMessageAction("Запись 1"),
//                new GenericMessageAction("Запись 2")
//            );
//        }
//        else if(true) {
//            // НЕ НОРМ
//            // java.util.ArrayList
//            List<IssueAction> list = Lists.newArrayList(
//                    new GenericMessageAction("Запись 1"),
//                    new GenericMessageAction("Запись 2")
//            );
//            return list;
//        } else {
//            ArrayList<IssueAction> list = new ArrayList<>();
//            list.add( new GenericMessageAction("Запись 1") );
//            list.add( new GenericMessageAction("Запись 2") );
//
//            return list;
//        }
    }
}
