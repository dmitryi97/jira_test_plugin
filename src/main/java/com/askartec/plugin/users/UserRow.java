package com.askartec.plugin.users;

import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import java.util.Date;


public class UserRow implements IssueAction {
    String login;
    String email;

    UserRow(String login, String email) {
        this.login = login;
        this.email = email;
    }





    public String getHtml() {
        return "<b>email: "+ this.email+ "</b>\n<b>login: "+ this.login +"</b>";
    }

    public Date getTimePerformed() {
        return new Date();
    }

    public boolean isDisplayActionAllTab() {
        return true;
    }
}
