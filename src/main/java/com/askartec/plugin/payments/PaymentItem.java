package com.askartec.plugin.payments;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import java.util.Date;

public class PaymentItem implements IssueAction {

    public PaymentItem() {}



    public String getHtml() {
        return "PAYMENT";
    }

    public Date getTimePerformed() {
        // Вернуть дату платежа
        return new Date();
    }

    public boolean isDisplayActionAllTab() {
        return true;
    }
}
