package com.askartec.plugin.payments;

 import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.tabpanels.GenericMessageAction;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel3;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import com.google.common.collect.Lists;

import java.sql.*;
import java.util.List;

//import org.postgresql.jdbc.;


// Этот класс указывается в XML
public class IssueTab extends AbstractIssueTabPanel3 {
    @Override
    public boolean showPanel(ShowPanelRequest showPanelRequest) {


        // Эта функция вызывается при открытии задачи, тут нужно проверить проект,
        // и если это CL, вернуть true для включения вкладки на страницу
        Issue curIssue = showPanelRequest.issue();
        String curProjectKey = curIssue.getProjectObject().getKey();

        return curProjectKey.equals("CL");
    }




    @Override
    public List<IssueAction> getActions(GetActionsRequest getActionsRequest) {
        String url = "jdbc:postgresql://192.168.0.166:5432/optima_jira_mobile";
        String username = "postgres";
        String password = "postgres";
        Connection conn = null;

//        return Lists.newArrayList(
//                new GenericMessageAction("Пока что платежей нет")
//        );

        try {
            conn = DriverManager.getConnection(url, username, password);

            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery("SELECT i.*, COALESCE(s.description, 'Неизвестная услуга') AS service_name,\n" +
                    "       (\n" +
                    "           SELECT json_agg(payments)\n" +
                    "           FROM (\n" +
                    "                    SELECT p.txn_id, p.txn_date, p.pay_amount, COALESCE(p.sender, 'Неизвестный отправитель') AS sender\n" +
                    "                    FROM ps_replenishment_payment p\n" +
                    "                    WHERE p.account = i.account AND p.service_id = i.service_id\n" +
                    "                ) AS payments\n" +
                    "       )\n" +
                    "FROM invoices_for_payment i\n" +
                    "LEFT JOIN rng_public_services s ON s.code = i.service_id\n" +
                    "WHERE issue_key = 'GAZ-5225';");

            while( rs.next() ){
                String login = rs.getString(1);
                String email = rs.getString(2);
                Date created = rs.getDate(3);

                // Обернуть поля в класс который поддерживает интерфейс IssueAction
                // Запихать этот объект в List
            }

            System.out.println("Connected to the PostgreSQL server successfully.");

            conn.close();

            return Lists.newArrayList(
                    new GenericMessageAction("Успешное соединение"),
                    new GenericMessageAction("Еще запись")
            );
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return Lists.newArrayList(
                new GenericMessageAction("Не удалось"),
                new GenericMessageAction("Соедениться с БД")
        );
    }
}
