package com.askartec.plugin.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.velocity.VelocityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//import com.atlassian.velocity.DefaultVelocityManager;

//import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Maps;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class resolution extends HttpServlet{
//    private static final Logger log = LoggerFactory.getLogger(resolution.class);

    private final VelocityManager velocityManager;

    public resolution(@JiraImport VelocityManager velocityManager) {
        this.velocityManager = velocityManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
//        this.velocityManager.getEncodedBody("/templates/", "servlet.vm", "UTF-8", Maps.newHashMap() );
//        Map<String, Object> context = Maps.newHashMap();
//        context.put("myVar", Math.random());

        String content = this.velocityManager.getBody( "/", "servlet.vm", "UTF-8", Maps.newHashMap() );
        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().write(content);
        resp.getWriter().close();
    }

}